module.exports = {
  bracketSpacing: true,
  jsxBracketSameLine: true,
  arrowParens: 'always',
  singleQuote: true,
  trailingComma: 'es5',
};
